'use strict';

/**
 * Подключаем модули
 */
var gulp        = require('gulp'),
    path        = require('path'),
    fs          = require('fs'),
    copy        = require('gulp-contrib-copy'),
    twig        = require('gulp-twig'),
    rigger      = require('gulp-rigger'),
    sass        = require('gulp-sass'),
    cleanCSS    = require('gulp-clean-css'),
    sourcemaps  = require('gulp-sourcemaps'),
    imagemin    = require('gulp-imagemin'),
    pngquant    = require('imagemin-pngquant'),
    uglify      = require('gulp-uglify'),
    watch       = require('gulp-watch'),
    spritesmith = require('gulp.spritesmith'),
    plumber     = require('gulp-plumber'),
    prefixer    = require('gulp-autoprefixer'),

    browserSync = require("browser-sync"),
    reload = browserSync.reload;


function getDateTime() {
    var now     = new Date();
    var year    = now.getFullYear();
    var month   = now.getMonth()+1;
    var day     = now.getDate();
    var hour    = now.getHours();
    var minute  = now.getMinutes();
    var second  = now.getSeconds();
    if(month.toString().length == 1) { var month = '0'+month; }
    if(day.toString().length == 1) { var day = '0'+day; }
    if(hour.toString().length == 1) { var hour = '0'+hour; }
    if(minute.toString().length == 1) { var minute = '0'+minute; }
    if(second.toString().length == 1) { var second = '0'+second; }
    var dateTime = year+'-'+month+'-'+day+' '+hour+':'+minute+':'+second;
    return dateTime;
}

/**
 * Путь к папке с доменом
 * @type {String}
 */
var path_domains = 'D:/Domains/';

/**
 * Домен на котором ведется разработка
 * Настройка проксирования
 * @type {String}
 */
// var developer_domain = 'avagarant.local';
var developer_domain = '';

/**
 * Пути по-умолчанию
 */
var default_filepath;

/**
 * Пути по-умолчанию если копируем сразу в сайт
 */
if (developer_domain != '')
{
    default_filepath = {
        source       : 'app-source/',
        build        : path_domains + developer_domain + '/Application/views/',
        build_assets : path_domains + developer_domain + '/Application/views/Assets/'
    }
}

/**
 * Пути если делаем только версту.
 * Копируем файлы в `app-build`
 */
else {
    default_filepath = {
        source       : 'app-source/',
        build        : 'app-build/',
        build_assets : 'app-build/Assets/'
    }
}

/**
 * Start BrowserSync server
 */
gulp.task('server', function ()
{
    if (developer_domain != '')
    {
        browserSync({
            port : 80,
            proxy : developer_domain
        });
    }

    else {
        browserSync.init({
            server: {
                baseDir: default_filepath.build
            }
        });
    }
});


var FilePaths = {

    source : {
        scripts : [ default_filepath.source + 'app-scripts/Application.js' ],
        styles  : [
            default_filepath.source + 'app-stylesheets/Vendor.scss',
            default_filepath.source + 'app-stylesheets/Application.scss'
        ],
        fonts   :   default_filepath.source + 'app-fonts/**/*',
        images  :   default_filepath.source + 'app-images/**/*.{png,jpg}',
        sprite  :   default_filepath.source + 'app-sprite/**/*',
        design  :   default_filepath.source + 'app-twig/**/**/*.twig'
    },

    build  : {
        maps    : 'maps',
        scripts : default_filepath.build_assets,
        styles  : default_filepath.build_assets,
        fonts   : default_filepath.build_assets + 'fonts',
        images  : default_filepath.build_assets + 'images',
        design  : default_filepath.build
    },

    watch : {
        scripts : default_filepath.source + 'app-scripts/**/*.js',
        styles  : default_filepath.source + 'app-stylesheets/**/*.scss',
        fonts   : default_filepath.source + 'app-fonts/**/*',
        images  : default_filepath.source + 'app-images/**/*.{png,jpg}',
        sprite  : default_filepath.source + 'app-sprite/**/*',
        design  : default_filepath.source + 'app-twig/**/*.twig'
    }
};


/**
 * Builds scripts
 */
gulp.task('js:build', function ()
{
    gulp.src(FilePaths.source.scripts)
        .pipe(plumber())
        .pipe(rigger())
        .pipe(sourcemaps.init())
        .pipe(uglify())
        .pipe(sourcemaps.write(FilePaths.build.maps))
        .pipe(plumber.stop())
        .pipe(gulp.dest(FilePaths.build.scripts))
        .pipe(reload({stream: true}));
});


/**
 * Builds styles
 */
gulp.task('styles:build', function ()
{
    gulp.src(FilePaths.source.styles)
        .pipe(plumber())
        .pipe(sourcemaps.init())
        .pipe(sass())
        .pipe(prefixer())
        .pipe(cleanCSS({compatibility: 'ie8'}))
        .pipe(sourcemaps.write(FilePaths.build.maps))
        .pipe(plumber.stop())
        .pipe(gulp.dest(FilePaths.build.styles))
        .pipe(reload({stream: true}));
});


/**
 * Builds images
 */
gulp.task('image:build', function ()
{
    gulp.src(FilePaths.source.images)
        .pipe(plumber())
        .pipe(imagemin({
            progressive: true,
            svgoPlugins: [{removeViewBox: false}],
            use: [pngquant()],
            interlaced: true
        }))
        .pipe(plumber.stop())
        .pipe(gulp.dest(FilePaths.build.images))
        .pipe(reload({stream: true}));
});

/**
 * Builds svg sprites
 */
gulp.task('sprite:build', function()
{
    var spriteData =
        gulp.src(FilePaths.source.sprite)
            .pipe(spritesmith({
                imgName: 'sprite.png',
                cssName: 'sprite.scss',
                cssFormat: 'scss',
                algorithm: 'binary-tree',
                padding: 20,
                imgPath: '../images/',
                cssVarMap: function(sprite) {
                    sprite.name = 's-' + sprite.name
                }
            }));

    spriteData.img.pipe(gulp.dest(FilePaths.build.images));
    spriteData.css.pipe(gulp.dest(FilePaths.build + 'app-stylesheets'))
        .pipe(reload({stream: true}));
});


/**
 * Copy fonts
 */
gulp.task('fonts:build', function()
{
    gulp.src(FilePaths.source.fonts)
        .pipe(gulp.dest(FilePaths.build.fonts))
        .pipe(copy())
        .pipe(reload({stream: true}))
});

/**
 * Copy Twigs design
 */
gulp.task('design:build', function ()
{
    /**
     * Если собираем на CMS то просто копируем файлы
     */
    if (developer_domain != '')
    {
        return gulp.src(FilePaths.source.design)
            .pipe(gulp.dest(FilePaths.build.design))
            .pipe(copy())
            .pipe(reload({stream: true}));
    }

    /**
     * Собираем HTML файлы
     */
    else {
        return gulp.src(FilePaths.source.design)
        // .pipe(console.log(getDateTime()))
        .pipe(twig({
            data : {
                build_date_gulp : getDateTime()
            },

            functions : [
                {
                    name : 'site_url',
                    func : function(string = '') { return '/' + string; }
                },
                {
                    name : 'assets',
                    func : function(string = '') { return '/Assets/' + string; }
                },
                {
                    name : 'form_open',
                    func : function(url) {
                        return '<form action="/'+ url +'" method="post">';
                    }
                },
                {
                    name : 'form_close',
                    func : function(url) {
                        return '</form>';
                    }
                },
            ],

            filters : [
                // {
                //     name : 'replace',
                //     func : function(string) {
                //         return string;
                //     }
                // }
            ]
        }))
        .pipe(gulp.dest(FilePaths.build.design))
        .pipe(reload({stream: true}));

    }


});


/**
 * Builds design website
 */
gulp.task('build', [
    'js:build',
    'sprite:build',
    'image:build',
    'styles:build',
    'fonts:build',
    'design:build'
]);

gulp.task('watch', function()
{
    watch([FilePaths.watch.scripts], function(event, cb)
    {
        gulp.start('js:build');
    });

    watch([FilePaths.watch.images], function(event, cb)
    {
        gulp.start('image:build');
    });

    watch([FilePaths.watch.sprite], function(event, cb)
    {
        gulp.start('sprite:build');
    });

    watch([FilePaths.watch.styles], function(event, cb)
    {
        gulp.start('styles:build');
    });

    watch([FilePaths.watch.fonts], function(event, cb)
    {
        gulp.start('fonts:build');
    });

    watch([FilePaths.watch.design], function(event, cb)
    {
        gulp.start('design:build');
    });
});

gulp.task('default', ['build', 'server', 'watch']);